import tiny_logger as log

log.LOG_LEVEL = 0

print("Logging: No Color")
log.LOG_COLOR = False
log.trace("Trace log")
log.debug("Debug log")
log.info("Info log")
log.warning("Warning log")
log.error("Error log")
log.critical("Critical log")

print("\nLogging: With Color")
log.LOG_COLOR = True
log.trace("Trace log")
log.debug("Debug log")
log.info("Info log")
log.warning("Warning log")
log.error("Error log")
log.critical("Critical log")
