# MIT License: https://gitlab.com/joeysbytes-python-snippets/tiny-logger/-/blob/main/LICENSE?ref_type=heads
# Copyright (c) 2024 Joey Rockhold

from datetime import datetime; import sys; LOG_LEVEL: int = 3; LOG_COLOR: bool = False


def _log(log_level: int, text: str) -> None:
    def fg(idx:int,txt:str)->str:return txt if not LOG_COLOR else f"\033[{idx%8+30+(60*(idx>7))}m{txt}\033[39m"
    def bg(idx:int,txt:str)->str:return txt if not LOG_COLOR else f"\033[{idx%8+40+(60*(idx>7))}m{txt}\033[49m"
    log_lvls = (("trace",5,0), ("debug",6,0), ("info ",7,0), ("Warn ",11,0), ("ERROR",11,1), ("*CRIT",15,5))
    if log_level >= LOG_LEVEL:
        print(f"[{datetime.now().strftime('%H:%M:%S')}] " +
              fg(log_lvls[log_level][1], bg(log_lvls[log_level][2], log_lvls[log_level][0])) + f" {text}",
              file=sys.stderr if log_level >= 3 else sys.stdout)


def trace(text: str) -> None: _log(0, text)
def debug(text: str) -> None: _log(1, text)
def info(text: str) -> None: _log(2, text)
def warning(text: str) -> None: _log(3, text)
def error(text: str) -> None: _log(4, text)
def critical(text: str) -> None: _log(5, text)
