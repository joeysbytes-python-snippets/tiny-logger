# Tiny Logger

## Example Usage

```python
import tiny_logger as log

log.debug("Log entry here")
```

## Log Reference

| Log Level | Log Function | Log Output | Output | Log Description                  |
|-----------|--------------|------------|--------|----------------------------------|
| 0         | trace        | trace      | stdout | Trace                            |
| 1         | debug        | debug      | stdout | Debug, Debugging                 |
| 2         | info         | info       | stdout | Info, Information, Informational |
| 3         | warning      | Warn       | stderr | Warn, Warning                    |
| 4         | error        | ERROR      | stderr | Error                            |
| 5         | critical     | CRIT       | stderr | Crit, Critical                   |

## Variables

| Variable  | Default Value | Description                     |
|-----------|---------------|---------------------------------|
| LOG_LEVEL | 3             | Minimum log level to output     |
| LOG_COLOR | False         | Whether to use ANSI color codes |
